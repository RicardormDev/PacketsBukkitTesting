package io.github.richthelord.Packets;

import java.lang.reflect.Constructor;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class Events implements Listener{
		
	static Main Main;
	public Events(Main i){
		Main = i;
	}
	
/*	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent e){
		if(e.getClickedBlock() == null)
			return;
		PacketPlayOutWorldParticles packet = new PacketPlayOutWorldParticles(
				EnumParticle.NOTE		,		//TYPE
				true,								//TRUE
				e.getClickedBlock().getX(),			//X coord
				e.getClickedBlock().getY(),			//Y coord
				e.getClickedBlock().getZ(),			//Z coord
				01,									//X OFFSET
				1,									//Y OFFSET
				01,									//Z OFFSET
				0,									//Speed
				1000,								//Number of particles
				null
		);	
		
		((CraftPlayer) e.getPlayer()).getHandle().playerConnection.sendPacket(packet);
		
	}*/
	
	@EventHandler
	public void onPlayerChat(AsyncPlayerChatEvent e) {
		if (e.getMessage().equals("title")) {
			// Title or subtitle, text, fade in (ticks), display time (ticks), fade out (ticks).
			try {
				Object enumTitle = getNMSClass("PacketPlayOutTitle").getDeclaredClasses()[0].getField("TITLE").get(null);
				Object chat = getNMSClass("IChatBaseComponent").getDeclaredClasses()[0].getMethod("a", String.class).invoke(null, "{\"text\":\"Welcome\"}");
				
				Constructor<?> titleConstructor = getNMSClass("PacketPlayOutTitle").getConstructor(getNMSClass("PacketPlayOutTitle").getDeclaredClasses()[0], getNMSClass("IChatBaseComponent"), int.class, int.class, int.class);
				Object packet = titleConstructor.newInstance(enumTitle, chat, 20, 40, 20);
				
				sendPacket(e.getPlayer(), packet);
			}
			
			catch (Exception e1) {
				e1.printStackTrace();
			}
		}
	}
	
	public void sendPacket(Player player, Object packet) {
		try {
			Object handle = player.getClass().getMethod("getHandle").invoke(player);
			Object playerConnection = handle.getClass().getField("playerConnection").get(handle);
			playerConnection.getClass().getMethod("sendPacket", getNMSClass("Packet")).invoke(playerConnection, packet);
		}
		
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public Class<?> getNMSClass(String name) {
		// org.bukkit.craftbukkit.v1_8_R3...
		String version = Bukkit.getServer().getClass().getPackage().getName().split("\\.")[3];
		try {
			return Class.forName("net.minecraft.server." + version + "." + name);
		}
		
		catch (ClassNotFoundException e) {
			e.printStackTrace();
			return null;
		}
	}

}
		
